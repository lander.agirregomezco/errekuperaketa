package testexam2;

import static org.junit.Assert.assertFalse;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import exam2.Exam2;



@RunWith(value= Parameterized.class)
public class TestExam2 {
	int n;
	
	@Parameters
	public static Collection<Object []> parameters(){
		return Arrays.asList(new Object[] [] {
			{2},
			{3}
		}
		);
	}
	
	public TestExam2(int n) {
		this.n=n;
		
	}
	
	@Test
	public void test() {
		Exam2 exam=new Exam2();
		assertFalse(exam.exam(n));
	}

}

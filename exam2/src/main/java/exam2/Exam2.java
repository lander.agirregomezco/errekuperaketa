package exam2;

public class Exam2 {
	
	public static boolean exam(int n){  
		int sum = 0;        
		int square = squareCalc(n);  
		while(square != 0)  
		{  
			int digit = getDigit(square);  
			sum = sum + digit;  
			square = square / 10;  
		}  
		if(n == sum)  
			return true;  
		else  
			return false; 
		}  
	
	public static int squareCalc(int n) {
		return n*n;
	}
	
	public static int getDigit (int s) {
		return s % 10;
	}

}
